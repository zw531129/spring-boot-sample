# Thymeleaf tips

## Overview

### Simple expressions:

- Variable Expressions: ${...}
- Selection Variable Expressions: *{...}
- Message Expressions: #{...}
- Link URL Expressions: @{...}
- Fragment Expressions: ~{...}

### Literals

- Text literals: 'one text', 'Another one!',…
- Number literals: 0, 34, 3.0, 12.3,…
- Boolean literals: true, false
- Null literal: null
- Literal tokens: one, sometext, main,…

### Text operations:

- String concatenation: +
- Literal substitutions: |The name is ${name}|

### Arithmetic operations:

- Binary operators: +, -, *, /, %
- Minus sign (unary operator): -
- Boolean operations:
- Binary operators: and, or
- Boolean negation (unary operator): !, not

### Comparisons and equality:

- Comparators: >, <, >=, <= (gt, lt, ge, le)
- Equality operators: ==, != (eq, ne)
- Conditional operators:
- If-then: (if) ? (then)
- If-then-else: (if) ? (then) : (else)
- Default: (value) ?: (defaultvalue)

### Special tokens:

- No-Operation: _

### All these features can be combined and nested:

- `'User is of type ' + (${user.isAdmin()} ? 'Administrator' : (${user.type} ?: 'Unknown'))`

## Tags & Objects

### Tags

- Escape Strings(Escape HTML tags): `<p th:text=${var}>Welcome to our &lt;b&gt;fantastic&lt;/b&gt; grocery store!</p>`
- Unescaped Strings: `<p th:utext=${var}>Welcome to our grocery store!</p>`
- Link Urls: `<script th:src="@{/js/app.js}"></script>`
- Src Urls: `<a href="details.html" th:href="@{/order/details(orderId=${o.id})}">view</a>`
- If statement: `<div th:if="${user.isAdmin() == false}"> </div>`
- Append Strings: `<span th:text="'The name of the user is ' + ${user.name}">`
- Form Binding and Action: `<form th:action="@{/target}" th:object="${user}">`
- Attribute Binding: `<input type="hidden" th:field="*{id}"/>`
- Set value: `<input type="text" th:value="something"/>`
- Iteration:

```
<tr th:each="prod,iterStat : ${prods}" th:class="${iterStat.odd}? 'odd'">
    <td th:text="${prod.name}">Onions</td>
    <td th:text="${prod.price}">2.41</td>
    <td th:text="${prod.inStock}? #{true} : #{false}">yes</td>
</tr>
```

### Basic Objects

- Get request variable: #request
- Get response variable: #response
- Get session variable: #session
- Get servletContext variable: #servletContext

### Utility Objects

- # execInfo: information about the template being processed.
- # messages: methods for obtaining externalized messages inside variables expressions, in the same way as they would be obtained using #{…} syntax.
- # uris: methods for escaping parts of URLs/URIs
- # conversions: methods for executing the configured conversion service (if any).
- # dates: methods for java.util.Date objects: formatting, component extraction, etc.
- # calendars: analogous to #dates, but for java.util.Calendar objects.
- # numbers: methods for formatting numeric objects.
- # strings: methods for String objects: contains, startsWith, prepending/appending, etc.
- # objects: methods for objects in general.
- # bools: methods for boolean evaluation.
- # arrays: methods for arrays.
- # lists: methods for lists.
- # sets: methods for sets.
- # maps: methods for maps.
- # aggregates: methods for creating aggregates on arrays or collections.
- # ids: methods for dealing with id attributes that might be repeated (for example, as a result of an iteration).
- `Today is: <span th:text="${#calendars.format(today,'dd MMMM yyyy')}">13 May 2011</span>`
