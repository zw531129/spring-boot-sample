package org.example.web;

import org.example.model.Msg;
import org.example.service.MsgService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/** Created by zw531 on 2017/1/5. Usage: */
@RestController
public class MsgController {

  @Autowired private MsgService msgService;

  @RequestMapping("/create")
  public String createTable() {
    msgService.createTable("msg");

    return "create table msg";
  }

  @RequestMapping("/add")
  public String add() {
    for (int i = 0; i < 10; i++) {
      Msg msg = new Msg();
      msg.setTitle("Msg " + String.valueOf(i) + " title");
      msg.setContent("Msg " + String.valueOf(i) + " content");
      msgService.add(msg);
    }

    return "batch add msgs";
  }

  @RequestMapping("/delete")
  public String listAndDelete() {
    List<Msg> msgs = msgService.getAllMsgs();

    for (Msg msg : msgs) {
      System.out.println("Msg id: " + msg.getId());
      System.out.println("Msg title: " + msg.getTitle());
      System.out.println("Msg content: " + msg.getContent());

      msgService.delete(msg.getId());
    }

    return "batch delete msgs";
  }

  public MsgService getMsgService() {
    return msgService;
  }

  public void setMsgService(MsgService msgService) {
    this.msgService = msgService;
  }
}
