package org.example.service;

import org.example.mapper.UserMapper;
import org.example.model.User;
import org.example.util.UUID;
import org.example.vo.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public UserDto add(UserDto userDto) {
        Long now = Instant.now().toEpochMilli();
        User tc = User.builder()
                .uid(UUID.fastUUID().toString().replaceAll("-", ""))
                .username(userDto.getUsername())
                .password(userDto.getPassword())
                .nickname(userDto.getNickname())
                .createTime(now)
                .updateTime(now)
                .build();
        try {
            userMapper.save(tc);
        } catch (Exception e) {
            log.error("save testcase failed. ", e);
            throw e;
        }
        // 回显数据
        userDto.setCreateTime(tc.getCreateTime());
        userDto.setUpdateTime(tc.getUpdateTime());
        userDto.setUid(tc.getUid());

        return userDto;
    }

    @Override
    public UserDto delete(String cid) {
        User tc = userMapper.get(cid);
        try {
            userMapper.delete(cid);
        } catch (Exception e) {
            log.error("delete testcase failed. ", e);
            throw e;
        }

        UserDto tcDto = UserDto.builder().build();
        BeanUtils.copyProperties(tc, tcDto);

        return tcDto;
    }

    @Override
    public UserDto get(String cid) {
        User tc;
        try {
            tc = userMapper.get(cid);
        } catch (Exception e) {
            log.error("get testcase failed. ", e);
            throw e;
        }
        UserDto tcDto = UserDto.builder().build();
        BeanUtils.copyProperties(tc, tcDto);

        return tcDto;
    }

    @Override
    public UserDto update(UserDto userDto) {
        User u = userMapper.get(userDto.getUid());
        u.setUsername(userDto.getUsername());
        u.setPassword(userDto.getPassword());
        u.setNickname(userDto.getNickname());
        u.setUpdateTime(Instant.now().toEpochMilli());
        try {
            userMapper.update(u);
        } catch (Exception e) {
            log.error("update testcase failed. ", e);
            throw e;
        }
        BeanUtils.copyProperties(u, userDto);

        return userDto;
    }

    @Override
    public List<UserDto> list() {
        List<User> users;
        try {
            users = userMapper.findAll();
        } catch (Exception e) {
            log.error("update testcase failed. ", e);
            throw e;
        }

        List<UserDto> userDtos = new ArrayList<>();
        for (User each : users) {
            UserDto ud = new UserDto(each);
            userDtos.add(ud);
        }

        return userDtos;
    }
}
