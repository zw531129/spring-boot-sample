//package org.example.consumer;
//
//import com.google.gson.JsonObject;
//import lombok.extern.slf4j.Slf4j;
//import org.apache.kafka.clients.consumer.ConsumerRecord;
//import org.example.util.JsonUtil;
//import org.springframework.kafka.annotation.KafkaListener;
//import org.springframework.stereotype.Component;
//
///**
// * @author zhangyf2595
// * @version 1.0
// * @date 2023/8/18
// **/
//@Component
//@Slf4j
//public class SpringKafkaConsumer {
//    @KafkaListener(topics = "${org.example.kafka.topic}")
//    public void receive(ConsumerRecord<String, String> records) {
//        String value = records.value();
//        log.info("kafka原始消息：{}", value);
//        JsonObject jObject = null;
//        try {
//            jObject = JsonUtil.fromJson(value, JsonObject.class);
//        } catch (Exception e) {
//            log.error("捕获非JSON消息：{}", value);
//        }
//        log.info("JSON消息: {}", jObject);
//    }
//}
