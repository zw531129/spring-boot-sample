# spring-boot-sample

归档spring-boot学习与使用过程中的案例.

## 案例一览

- spring-boot-actuator

  - actuator配置案例

- spring-boot-kickstart-rest

    - 使用最少代码构建RestFul Web应用

- spring-boot-kickstart-thymeleaf

  - 使用thymeleaf渲染视图

- spring-boot-data-jdbc

    - 使用JdbcTemplate进行数据访问

- spring-boot-data-jpa

    - 使用JPA(Hibernate)进行数据访问

- spring-boot-mybatis

    - 使用mybatis访问数据库

- spring-boot-redis

    - 使用redisTemplate进行数据访问

- spring-boot-ssl

  - 整合证书相关安全

- spring-boot-cli-kafka

  - 使用kafka接收与发送消息

- spring-boot-webflux

  - 使用webflux模型进行高吞吐处理

- spring-boot-websocket

  - 使用websocket模型实现双工通信



