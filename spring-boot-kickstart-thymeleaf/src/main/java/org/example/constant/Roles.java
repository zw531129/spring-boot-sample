package org.example.constant;

import org.example.model.Role;

import java.util.ArrayList;
import java.util.List;

public enum Roles {
    EMPLOYEE(1, "Employee"),
    MANAGER(2, "Manager"),
    ADMINISTRATOR(3, "Administrator");

    private final int rid;
    private final String name;

    Roles(int rid, String name) {
        this.rid = rid;
        this.name = name;
    }

    public static List<Role> getAll() {
        List<Role> roles = new ArrayList<>();

        for (Roles each : Roles.values()) {
            Role role = new Role();
            role.setRid(each.getRid());
            role.setName(each.getName());
            roles.add(role);
        }

        return roles;
    }

    public static Role getById(String roleId) {
        Role role = new Role();
        for (Roles each : Roles.values()) {
            int rid = each.getRid();
            String name = each.getName();
            if (Integer.parseInt(roleId) != rid) {
                continue;
            }
            role.setRid(rid);
            role.setName(name);
        }

        return role;
    }

    public int getRid() {
        return rid;
    }

    public String getName() {
        return name;
    }
}
