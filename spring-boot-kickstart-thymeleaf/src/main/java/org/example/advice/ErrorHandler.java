package org.example.advice;

import org.example.exception.InternalException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@ControllerAdvice
public class ErrorHandler {
    /**
     * 处理所有抛出MyException异常的方法
     *
     * @param response response
     * @throws IOException ioe
     */
    @ExceptionHandler(value = InternalException.class)
    public void unknownHandler(HttpServletResponse response) throws IOException {
        response.sendRedirect("/error/500");
    }
}
