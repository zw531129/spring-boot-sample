package org.example.service;

import org.example.vo.UserDto;

import java.util.List;

/**
 * 用户类业务逻辑
 *
 */
public interface UserService {
    /**
     *
     * @param userDto
     * @return
     */
    UserDto add(UserDto userDto);

    /**
     *
     * @param uid
     * @return
     */
    UserDto delete(String uid);

    /**
     *
     * @param uid
     * @return
     */
    UserDto get(String uid);

    /**
     *
     * @param userDto
     * @return
     */
    UserDto update(UserDto userDto);

    /**
     *
     * @return
     */
    List<UserDto> list();

}
