package org.example.mapper;

import org.example.model.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserMapper {
    void save(User user);

    User get(String pid);

    void delete(String pid);

    void update(User user);

    List<User> findAll();
}
