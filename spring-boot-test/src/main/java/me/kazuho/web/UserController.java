package me.kazuho.web;

import me.kazuho.model.User;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by zw531 on 2018/1/3.
 * Usage:
 */
@RestController
public class UserController {

    private Map<Long, User> users = new ConcurrentHashMap<Long, User>();

    @RequestMapping(value = "/user/list")
    public List<User> list(){
        List<User> userList = new ArrayList<User>(users.values());
        return userList;
    }

    @RequestMapping(value = "/user/{id}")
    public String get(@PathVariable Long id){
        User user = users.get(id);
        return user.toString();
    }

    @RequestMapping(value = "/user/add")
    public String add(@ModelAttribute User user){
        users.put(user.getId(), user);
        return "add user: " + user.getId() + " success";
    }

    @RequestMapping(value = "/user/{id}/delete")
    public String delete(@PathVariable Long id){
        users.remove(id);
        return "remove user: " + id + " success";
    }

    @RequestMapping(value = "/user/{id}/update")
    public String update(@PathVariable Long id, @ModelAttribute User user){
        User tmp = users.get(id);
        tmp.setName(user.getName());
        tmp.setAge(user.getAge());
        users.put(tmp.getId(), tmp);

        return "update user: " + tmp.getId() + " success";
    }
}
