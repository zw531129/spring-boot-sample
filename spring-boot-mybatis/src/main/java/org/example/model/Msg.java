package org.example.model;

/** Created by zw531 on 2017/12/6. Usage: */
public class Msg {
  private Long id;
  private String title;
  private String content;

  @Override
  public String toString() {
    return "Msg{" + "id=" + id + ", title='" + title + '\'' + ", content='" + content + '\'' + '}';
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
