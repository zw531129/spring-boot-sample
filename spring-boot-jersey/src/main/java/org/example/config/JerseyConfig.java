package org.example.config;

import org.example.endpoint.AwesomeEndPoint;
import org.glassfish.jersey.server.ResourceConfig;
import org.springframework.stereotype.Component;

/** 注册映射的JerseyUrl */
@Component
public class JerseyConfig extends ResourceConfig {
  public JerseyConfig() {
    register(AwesomeEndPoint.class);
  }
}
