package me.kazuho.web;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import me.kazuho.dao.Repository;
import me.kazuho.model.Department;
import me.kazuho.model.Employee;
import me.kazuho.model.Role;

/**
 * Created by zw531 on 2018/4/9. Usage:
 */
@Controller
public class EmployeeController {

  private static String i2s(Integer num) {
    switch (num) {
      case 1:
        return "I";
      case 2:
        return "II";
      case 3:
        return "III";
      default:
        return "Unknown";
    }
  }

  @RequestMapping(value = "/list", method = RequestMethod.GET)
  public String list(Model model) {
    List<Employee> employees = Repository.getEmployees();
    model.addAttribute("employees", employees);

    return "employee/list";
  }

  @RequestMapping(value = "/add", method = RequestMethod.GET)
  public String add(Model model) {
    Employee employee = new Employee();
    model.addAttribute("employee", employee);
    List<Department> departments = Repository.getDepartments();
    model.addAttribute("departments", departments);

    return "employee/view";
  }

  @RequestMapping(value = "/process", method = RequestMethod.POST)
  public String process(@ModelAttribute Employee employee,
                        @RequestParam("deptId") Integer deptId,
                        @RequestParam("roleIds") String roleIds,
                        BindingResult bindingResult,
                        Model model) {
    if (bindingResult.hasErrors()) {
            /*
                send roles and ids back
             */

      return "employee/view";
    }


    Department department = Repository.getDepartmentById(deptId);
    employee.setDepartment(department);
    List<Employee> employees = Repository.getEmployees();

    String[] rawIds = roleIds.split(",");
    Set<Role> roles = new HashSet<Role>();

    for (String s : rawIds) {
      //  fetch roles from database, then set to employee
    }

    employee.setRoles(roles);
    employees.add(employee);

    return "redirect:/list";
  }

  @RequestMapping("/init")
  @ResponseBody
  public String init() {
    for (int i = 1; i <= 3; i++) {
      Department dept = new Department();
      dept.setId(i);
      dept.setName("R&D " + i2s(i));
      List<Department> departments = Repository.getDepartments();
      departments.add(dept);
    }

    return "init departments done.";
  }

}
