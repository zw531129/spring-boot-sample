#!/bin/bash
# shell执行失败就退出
set -e
# 根据参数选择是安装还是删除
ACTION=$1
# 获取当前路径
WORKSPACE=$(cd $(dirname $(dirname $0../)); pwd)
JRE_EXEC=/data/third-part/jre/bin/java
SERVICE_NAME=foobar
SERVICE_JAR="$SERVICE_NAME.jar"
LOG_DIR=$WORKSPACE/log

# 创建日志路径
mkdir -p $LOG_DIR


function log_info() {
    if [ ! -d $LOG_DIR ]
    then
        mkdir -p $LOG_DIR
        touch $LOG_DIR/appctl.log
    fi

    DATE_N=`date "+%Y-%m-%d %H:%M:%S"`
    USER_N=`whoami`
    echo "${DATE_N} ${USER_N} execute $0 [INFO] $@ " >> $LOG_DIR/appctl.log
}

function log_error() {
    if [ ! -d $LOG_DIR ]
    then
        mkdir -p $LOG_DIR
        touch $LOG_DIR/appctl.log
    fi

    DATE_N=`date "+%Y-%m-%d %H:%M:%S"`
    USER_N=`whoami`
    echo "${DATE_N} ${USER_N} execute $0 [ERROR] $@ " >> $LOG_DIR/appctl.log
}

function start() {
  # debug使用-XX:+UseG1GC -Xms4096m -Xmx4096m -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC -Xloggc:./gc.log
    nohup $JRE_EXEC -XX:+UseG1GC -Xms4096m -Xmx4096m -jar $WORKSPACE/$SERVICE_JAR > /dev/null 2>&1 &
    log_info "service ${SERVICE_NAME} started"
}

function stop() {
    ps -ef | grep $SERVICE_JAR | grep -v "grep" | awk '{print $2}' | xargs kill -9
    if [ $? -ne 0 ]
    then
        log_error "stop $SERVICE_NAME failed"
        exit 1
    fi
    log_info "${SERVICE_NAME} killed"
}

function install() {
    log_info "installing $SERVICE_NAME"
}

function uninstall() {
    log_info "stopping $SERVICE_NAME"
    stop
    log_info "uninstalling $SERVICE_NAME"
    rm -rf $WORKSPACE/$SERVICE_NAME
    log_info "$SERVICE_NAME uninstalled"
}


case $MODE in
    install)
        install
        ;;
    uninstall)
        uninstall
        ;;
    start)
        start
        ;;
    stop)
        stop
        ;;
    restart)
        restart
        ;;
    *)
        echo "choose function in
                (install_jre|uninstall_jre|
                 install_scala|uninstall_scala|
                 install_zookeeper|uninstall_zookeeper|
                 install_kafka|uninstall_kafka|
                 install_clickhouse|install_clickhouse)"
esac