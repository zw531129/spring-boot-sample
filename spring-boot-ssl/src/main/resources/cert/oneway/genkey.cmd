# 生成服务器私钥+公钥
keytool -genkey -alias server -keypass 123456 -keyalg RSA -keysize 1024 -validity 365 -storepass 123456 -storetype PKCS12 -keystore ./server.p12
# 导出服务器公钥
keytool -export -alias server -keystore ./server.p12 -storetype PKCS12 -keypass 123456 -file ./server.cer