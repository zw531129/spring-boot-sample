package me.kazuho.service;

import java.util.List;

import me.kazuho.model.Msg;

/**
 * Created by zw531 on 2018/1/4. Usage:
 */
public interface MsgService {

  void addMsg(Msg msg);

  List<Msg> getAllMsgs();

  void deleteMsg(Long id);

  Msg findByTitle(String title);

}
