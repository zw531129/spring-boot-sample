package me.kazuho.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import me.kazuho.util.LogUtil;

/**
 * Created by zw531 on 2018/1/8. Usage:
 */
@RestController
@RequestMapping("/log")
public class LogController {
  private static final Logger LOGGER = LoggerFactory.getLogger(LogController.class);

  @RequestMapping("/debug")
  public String debug() {
    LOGGER.debug("-------DEBUG LOG-------");
    return "debug log started";
  }

  @RequestMapping("/info")
  public String info() {
    LOGGER.info("-------INFO LOG-------");
    return "info log started";
  }

  @RequestMapping("/warn")
  public String warn() {
    LOGGER.warn("-------WARN LOG-------");
    return "warn log started";
  }

  @RequestMapping("/error")
  public String error() {
    LOGGER.error("-------ERROR LOG-------");
    return "error log started";
  }

  @RequestMapping("/various")
  public String createLogs() {
    LogUtil.createLog();
    return "create logs";
  }
}
