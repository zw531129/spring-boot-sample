package org.example.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Greeting {
  private String words;
  private List<String> numbers = new ArrayList<>();

  @Override
  public String toString() {
    return "Greeting{" + "words='" + words + '\'' + '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    Greeting greeting = (Greeting) o;
    return Objects.equals(words, greeting.words);
  }

  @Override
  public int hashCode() {

    return Objects.hash(words);
  }

  public String getWords() {
    return words;
  }

  public void setWords(String words) {
    this.words = words;
  }

  public List<String> getNumbers() {
    return numbers;
  }

  public void setNumbers(List<String> numbers) {
    this.numbers = numbers;
  }
}
