package org.example.dao;

import org.example.model.Msg;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/** Created by zw531 on 2018/1/4. Usage: */
@Repository
@CacheConfig
public interface MsgDao extends JpaRepository<Msg, Long> {
  @Cacheable
  Msg findByTitle(String title);
}
