package me.kazuho.service;

import me.kazuho.dao.UserDao;
import me.kazuho.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

  @Autowired
  private UserDao userDao;

  @Override
  public User emptyUser() {
    return userDao.getEmptyUser();
  }
}
