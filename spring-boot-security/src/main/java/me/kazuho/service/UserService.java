package me.kazuho.service;

import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * Created by zw531 on 2018/2/18. Usage:
 */
public interface UserService extends UserDetailsService {
}
