package org.example.framework.exception;

public class GenericCryptoException extends RuntimeException {
    public GenericCryptoException(String message, Throwable cause) {
        super(message, cause);
    }

    public GenericCryptoException(Throwable cause) {
        super(cause);
    }
}
