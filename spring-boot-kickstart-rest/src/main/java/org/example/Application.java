package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * spring-boot启动类
 *
 * @author KazuHo
 * @version 1.0
 * @since 2021-12-12
 */
@SpringBootApplication
@EnableScheduling
@EnableAsync // 启用异步, 避免线程sleep后不再启动新的定时任务
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}