package org.exmaple.web;

import org.exmaple.dao.UserDao;
import org.exmaple.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/** Created by zw531 on 2018/1/10. Usage: */
@RestController
public class UserController {
  @Autowired private UserDao userService;

  @RequestMapping(value = "add")
  public String add() {
    User user = new User();
    user.setUsername("Bob");
    user.setPassword("s3cret");
    userService.saveOneUser(user.getUsername(), user.getPassword());

    return "user add finish";
  }

  @RequestMapping(value = "/update")
  public String update() {
    User user = userService.getOneUser(1);
    user.setPassword("password");
    userService.saveOneUser(user.getUsername(), user.getPassword());

    return "user update finish";
  }

  @RequestMapping(value = "/list")
  public String list() {
    List<User> users = userService.getAllUsers();
    for (User user : users) {
      System.out.println("--- " + user.getId() + " ---");
      System.out.println("--- " + user.getUsername() + " ---");
      System.out.println("--- " + user.getPassword() + " ---");

      userService.deleteOneUser(user.getId());
    }

    return "user list and delete finish";
  }

  @RequestMapping(value = "/create")
  public void createTable() {
    userService.createTable();
  }
}
