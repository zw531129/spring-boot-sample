package org.example.web;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import org.example.handler.WebSocketHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @author zhangyf2595
 * @version 1.0
 * @date 2023/8/21
 **/
@RestController
public class WebSocketController {
    @Autowired
    private WebSocketHandler handler;

    @PostMapping("/sendOnce/{uid}")
    public void sendOnce(@PathVariable String uid, @RequestBody Map<String, Object> body) {
        handler.sendOnce(uid, JSON.toJSONString(body));
    }

    @PostMapping("/broadcast")
    public void broadcast(@RequestBody Map<String, Object> body) {
        handler.broadcast(JSON.toJSONString(body));
    }

    @PostMapping("/sendSome/{uids}")
    public void sendSome(@PathVariable String[] uids, @RequestBody Map<String, Object> body) {
        handler.sendSome(uids, JSON.toJSONString(body));
    }
}
