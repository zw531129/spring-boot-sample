package org.example.bean;

import com.alibaba.druid.pool.DruidDataSource;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


@Configuration
@Slf4j
public class ClickHouseDataSourcePool {
    @Value("${clickhouse.driver}")
    private String driver;
    @Value("${clickhouse.url}")
    private String url;
    @Value("${clickhouse.username}")
    private String username;
    @Value("${clickhouse.password}")
    private String password;
    @Value("${clickhouse.initial-size}")
    private Integer initialSize;
    @Value("${clickhouse.max-active}")
    private Integer maxActive;
    @Value("${clickhouse.min-idle}")
    private Integer minIdle;
    @Value("${clickhouse.max-wait}")
    private Integer maxWait;

    @Bean
    public List<JdbcTemplate> clickHouseDataSourcePool() {
        List<JdbcTemplate> pool = new ArrayList<>();
        for (String each : url.split(",")) {
            DruidDataSource dataSource = new DruidDataSource();
            dataSource.setDriverClassName(driver);
            dataSource.setUrl(each);
            dataSource.setUsername(username);
            dataSource.setPassword(password);
            dataSource.setMinIdle(minIdle);
            dataSource.setMaxActive(maxActive);
            dataSource.setMaxWait(maxWait);
            pool.add(new JdbcTemplate(dataSource));
        }

        return Collections.unmodifiableList(pool);
    }
}