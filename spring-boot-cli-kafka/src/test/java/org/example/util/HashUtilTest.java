package org.example.util;

import org.example.framework.util.HashUtil;
import org.junit.Assert;
import org.junit.Test;

public class HashUtilTest {
    @Test
    public void testConsistentHash() {
        Assert.assertEquals(3, HashUtil.consistentHash("I'm message.".hashCode(), 4));
        Assert.assertEquals(0, HashUtil.consistentHash("Shut your fucking mouth up!".hashCode(), 4));
    }
}
