package org.example.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
public class ClientController {

    @Autowired
    RestTemplate restTemplate;

    @GetMapping("/trustAll")
    public ResponseEntity<?> trustAll() {
        String resp = restTemplate.getForEntity("https://httpbin.org/", String.class).getBody();

        return new ResponseEntity<>(resp, HttpStatus.OK);
    }
}
