package org.example.web;

import org.example.constant.Departments;
import org.example.constant.Roles;
import org.example.exception.DataProcessException;
import org.example.model.Address;
import org.example.model.Department;
import org.example.model.Employee;
import org.example.model.Role;
import org.example.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by zw531 on 2018/4/9. Usage:
 */
@Controller
public class EmployeeController {
    @Autowired
    private EmployeeService employeeService;


    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public String list(Model model) {
        List<Employee> employees;

        try {
            employees = employeeService.findAll();
        } catch (DataProcessException e) {
            employees = Collections.emptyList();
        }

        model.addAttribute("employees", employees);

        return "employee/list";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model) {
        Employee employee = new Employee();
        model.addAttribute("employee", employee);
        Address address = new Address();
        model.addAttribute("address", address);
        List<Department> departments = Departments.getAll();
        model.addAttribute("departments", departments);
        System.out.println(departments);
        List<Role> roles = Roles.getAll();
        model.addAttribute("roles", roles);
        System.out.println(roles);

        return "employee/view";
    }

    @RequestMapping(value = "/process", method = RequestMethod.POST)
    public String process(
            @ModelAttribute Employee employee,
            @RequestParam("deptId") Integer deptId,
            @RequestParam("roleIds") String roleIds,
            BindingResult bindingResult,
            Model model) {
        if (bindingResult.hasErrors()) {
            // send roles and ids back
            return "employee/view";
        }
        Department department = Departments.getById(deptId);
        employee.setDepartment(department);
        Set<Role> roles = new HashSet<>();
        for (String roleId : roleIds.split(",")) {
            Role role = Roles.getById(roleId);
            roles.add(role);
        }
        employee.setRoles(roles);
        employee.setJoinedTime(LocalDate.now());
        Employee result = employeeService.save(employee);
        model.addAttribute("employee", result);
        // return to your new employee ?
        // return "employee/view";
        // or return to total list ?
        return "redirect:/list";
    }
}
