package org.example.service;

import org.example.mapper.MsgMapper;
import org.example.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/** Created by zw531 on 2018/1/11. Usage: */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class MsgServiceImpl implements MsgService {
  @Autowired private MsgMapper msgMapper;

  @Override
  public List<Msg> getAllMsgs() {
    return msgMapper.findAll();
  }

  @Override
  public Msg findById(Long id) {
    return msgMapper.findById(id);
  }

  @Override
  public Msg findByTitle(String title) {
    return msgMapper.findByTitle(title);
  }

  @Override
  public void delete(Long id) {
    msgMapper.delete(id);
  }

  @Override
  public void add(Msg msg) {
    msgMapper.add(msg);
  }

  @Override
  public void update(Msg msg) {
    msgMapper.update(msg);
  }

  @Override
  public void createTable(String tableName) {
    msgMapper.createTable(tableName);
  }

  public MsgMapper getMsgMapper() {
    return msgMapper;
  }

  public void setMsgMapper(MsgMapper msgMapper) {
    this.msgMapper = msgMapper;
  }
}
