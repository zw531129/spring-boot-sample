package me.kazuho.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;

import me.kazuho.service.UserServiceImpl;

/**
 * Created by zw531 on 2017/12/25. Usage:
 */
@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
  @Bean
  UserDetailsService customUserService() {
    return new UserServiceImpl();
  }

  @Override
  public void configure(WebSecurity web) throws Exception {
    web.ignoring().antMatchers("/bootstrap/**", "/js/**", "/custom/**");
  }

  @Override
  protected void configure(HttpSecurity http) throws Exception {
    http.authorizeRequests()
            .antMatchers("/home/**").hasRole("ADMIN")
            .anyRequest().authenticated()
            .and()
            .formLogin()
            .loginPage("/auth/login")
            .failureUrl("/auth/login?error")
            .permitAll()
            .and()
            .rememberMe()
            .tokenValiditySeconds(60 * 60 * 24 * 7)
            .key("s3cret")
            .and()
            .logout().permitAll();
  }

  @Override
  protected void configure(AuthenticationManagerBuilder auth) throws Exception {
    auth.userDetailsService(customUserService());
  }
}
