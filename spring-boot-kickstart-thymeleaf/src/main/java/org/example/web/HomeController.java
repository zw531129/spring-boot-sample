package org.example.web;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/** Created by zw531 on 2018/1/3. Usage: */
@Controller
public class HomeController {
  @RequestMapping("/index")
  public String home() {
    return "home/index";
  }
}
