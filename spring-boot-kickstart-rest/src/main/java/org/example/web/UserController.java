package org.example.web;

import org.example.exception.permission.*;
import org.example.service.UserService;
import org.example.vo.UserDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 权限ctrl
 */
@RestController
@RequestMapping("/testcase")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @PostMapping("/add")
    public UserDto add(@Validated @RequestBody UserDto body) {
        UserDto resp;
        try {
            resp = userService.add(body);
        } catch (Exception e) {
            throw new UserAddException(e);
        }
        return resp;
    }

    @DeleteMapping("/{uid}/delete")
    public UserDto delete(@PathVariable String uid) {
        UserDto resp;
        try {
            resp = userService.delete(uid);
        } catch (Exception e) {
            throw new UserDeleteException(e);
        }

        return resp;
    }

    @PutMapping("/update")
    public UserDto update(@Validated @RequestBody UserDto body) {
        UserDto resp;
        try {
            resp = userService.update(body);
        } catch (Exception e) {
            throw new UserUpdateException(e);
        }

        return resp;
    }

    @GetMapping("/{uid}/detail")
    public UserDto detail(@PathVariable String uid) {
        UserDto resp;
        try {
            resp = userService.get(uid);
        } catch (Exception e) {
            throw new UserDetailException(e);
        }

        return resp;
    }

    @GetMapping("/list")
    public List<UserDto> list() {
        List<UserDto> resp;
        try {
            resp = userService.list();
        } catch (Exception e) {
            throw new UserListException(e);
        }

        return resp;
    }
}
