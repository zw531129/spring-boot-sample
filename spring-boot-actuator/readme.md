# spring-boot-actuator

## Actuator监控

Actuator是spring-boot提供的对应用系统的自省和监控的集成功能，可以查看应用配置的详细信息。一般配合安全控制的组件spring-boot-start-security使用，微服务的运行状态信息的泄露可能成为网络黑客攻击的契机。

## Actuator的RESTful接口

Actuator提供了丰富的REST接口供用户查看微服务的运行状态, 主要分为**应用配置**、**度量指标**、**操作控制**三大类。

- 应用配置类：应用运行期的配置信息、SpringBean信息、环境信息等静态信息。
- 度量指标类：应用运行期的堆栈、请求链、健康指标等动态信息。
- 操作控制类：用户关闭应用等操作信息。

Actuator提供的所有接口可以通过访问`http://ip:port/actuator`获取，下表列出了一些常用的监控接口。
| HTTP 方法 | 路径 | 描述 |
| :-------- | :-------------- | :----------------------------------------------------------- |
| GET | /auditevents | 显示应用暴露的审计事件 (比如认证进入、订单失败)              |
| GET | /beans | 描述应用程序上下文里全部的 Bean，以及它们的关系 |
| GET | /conditions | 提供一份自动配置生效的条件情况，记录哪些自动配置条件通过了，哪些没通过 |
| GET | /configprops | 描述配置属性(包含默认值)如何注入Bean |
| GET | /env | 获取全部环境属性 |
| GET | /health | 报告应用程序的健康指标，这些值由 HealthIndicator 的实现类提供 |
| GET | /heapdump | dump 一份应用的 JVM 堆信息 |
| GET | /httptrace | 显示最近100个HTTP请求与响应的Trace |
| GET | /info | 获取应用程序的定制信息，这些信息由info打头的属性提供 |
| GET | /logfile | 返回log file中的内容(如果 logging.file 或者 logging.path 被设置) |
| GET | /loggers | 显示和修改配置的loggers |
| GET | /metrics | 报告各种应用程序度量信息，比如内存用量和HTTP请求计数 |
| GET | /scheduledtasks | 展示应用中的定时任务信息 |
| GET | /sessions | 如果我们使用了 Spring Session 展示应用中的 HTTP sessions 信息 |
| POST | /shutdown | 关闭应用程序，要求endpoints.shutdown.enabled设置为true |
| GET | /mappings | 描述全部的 URI路径，以及它们和控制器(包含Actuator端点)的映射关系 |
| GET | /threaddump | 获取线程活动的快照 |

## 自定义健康检查项

Actuator也提供了自定义健康检查项的能力，假定现在有一个场景你需要连接绑定在某个IP上的Redis，你需要检查你所连接的IP是否可达，这里就可以使用Actuator进行监控。
Actuator支持开发者实现**HealthIndicator**接口自定义健康检查项，自定义的健康检查项会被收纳到**/heatlth**接口下,
如下就是一个检测IP是否可达的实现。

```java
/**
 * 通过实现HealthIndicator来自定义健康检查项目 包括但不限于: 端口侦听/进程存活等
 */
@Component
public class CustomHealthCheck implements HealthIndicator {
    private static final String TARGET_IP = "255.255.255.255";

    @Override
    public Health health() {
        boolean isReachable = checkConnection();
        if (!isReachable) {
            return Health.down()
                    .withDetail("error", "check connection to " + TARGET_IP + " failed.")
                    .build();
        }

        return Health.up()
                .withDetail("message", "check connection to " + TARGET_IP + " success.")
                .build();
    }

    private boolean checkConnection() {
        // ping timeout
        int timeOut = 3000;
        boolean status;
        try {
            status = InetAddress.getByName(TARGET_IP).isReachable(timeOut);
            if (!status) {
                return false;
            }
        } catch (IOException e) {
            // TODO: 生产环境别这么写, 会被问候祖宗的。
            e.printStackTrace();
        }

        return true;
    }
}
```
