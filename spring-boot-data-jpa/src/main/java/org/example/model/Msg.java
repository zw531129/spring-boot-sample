package org.example.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

/**
 * Created by zw531 on 2018/1/4. Usage:
 */
@Entity
public class Msg {
  @Id
  @GeneratedValue
  private Long id;
  private String title;
  private String content;

  public Msg() {
  }

  public Msg(String title, String content) {
    this.title = title;
    this.content = content;
  }

  @Override
  public String toString() {
    return "Msg{" + "id=" + id + ", content='" + content + '\'' + '}';
  }

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getContent() {
    return content;
  }

  public void setContent(String content) {
    this.content = content;
  }
}
