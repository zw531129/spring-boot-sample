//package org.example.bean;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.kafka.core.KafkaOperations;
//import org.springframework.kafka.listener.CommonErrorHandler;
//import org.springframework.kafka.listener.DeadLetterPublishingRecoverer;
//import org.springframework.kafka.listener.DefaultErrorHandler;
//import org.springframework.kafka.support.converter.JsonMessageConverter;
//import org.springframework.kafka.support.converter.RecordMessageConverter;
//import org.springframework.util.backoff.FixedBackOff;
//
///**
// * @author zhangyf2595
// * @version 1.0
// * @date 2024/7/29
// **/
//@Configuration
//public class KafkaConfig {
//    /**
//     * 通用错误处理
//     *
//     * @param template
//     * @return
//     */
//    @Bean
//    public CommonErrorHandler errorHandler(KafkaOperations<Object, Object> template) {
//        return new DefaultErrorHandler(
//                new DeadLetterPublishingRecoverer(template), new FixedBackOff(1000L, 2));
//    }
//
//    /**
//     *
//     * @return
//     */
//    @Bean
//    public RecordMessageConverter converter() {
//        return new JsonMessageConverter();
//    }
//}
