#!/bin/bash

set -e
NOW=`date "+%Y-%m-%d %H:%M:%S"`
USER=`whoami`
WORKSPACE=$(cd $(dirname $(dirname $0../)); pwd)
JRE_EXEC=/data/third-part/jre/bin/java
SERVICE_NAME=foobar
SERVICE_JAR="$SERVICE_NAME.jar"
LOG_DIR=$WORKSPACE/log

# 创建日志路径
mkdir -p $LOG_DIR

function stop() {
  ps -ef | grep $SERVICE_JAR | grep -v "grep" | awk '{print $2}' | xargs kill -9
  echo "${NOW} ${USER} ${SERVICE_NAME} killed"
}

function start() {
  # debug使用-XX:+UseG1GC -Xms4096m -Xmx4096m -XX:+PrintGCDetails -XX:+PrintGCTimeStamps -XX:+PrintGCDateStamps -XX:+PrintHeapAtGC -Xloggc:./gc.log
  nohup $JRE_EXEC -XX:+UseG1GC -Xms4096m -Xmx4096m -jar $WORKSPACE/$SERVICE_JAR > /dev/null 2>&1 &
  echo "${NOW} ${USER} service ${SERVICE_NAME} started"
}

# crontab -e
# */5 * * * * /bin/bash /data/gateway-service/gateway-collector/bin/keep_alive.sh
pid=$(ps -ef | grep "$SERVICE_JAR" | grep -v "grep" | awk '{print $2}');
if [ -z "${pid}" ];
then
    echo "${NOW} wake up $WORKSPACE/$SERVICE_NAME now" >> $WORKSPACE/$LOG_DIR/cron.log
    start
    echo "${NOW} $WORKSPACE/$SERVICE_NAME awake" >> $WORKSPACE/$LOG_DIR/cron.log
else
    echo "${NOW} $WORKSPACE/$SERVICE_NAME alive" >> $WORKSPACE/$LOG_DIR/cron.log
fi
