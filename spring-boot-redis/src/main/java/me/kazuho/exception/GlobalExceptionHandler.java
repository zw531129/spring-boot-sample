package me.kazuho.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.NoHandlerFoundException;


@RestControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler(value = RedisOperationException.class)
  public ResponseEntity<String> redisExeptionHandler(Exception e) {
    String message = "operation for redis failed!";
    return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
  }

  @ExceptionHandler(value = Exception.class)
  public ResponseEntity<String> unknownExeptionHandler(Exception e) {
    if (e instanceof NoHandlerFoundException) {
      String message = "unknown request url!";
      return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    String message = "server unknown error!";
    return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
  }

}
