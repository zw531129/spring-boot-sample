package org.example;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * 启动一个WebServer做端到端功能测试
 */
@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class ApplicationTest {
    @Autowired
    TestRestTemplate restTemplate;

    @Test
    public void greet() {
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Sec-Key", "woods");
        HttpEntity<String> requestEntity = new HttpEntity<>(null, headers);
        ResponseEntity<String> resEntity = restTemplate.exchange("/welcome?name=Ivan", HttpMethod.GET, requestEntity, String.class);
        assertThat(resEntity.getBody()).isEqualTo("{\"code\":200,\"message\":\"welcome Ivan, your key is [woods]\",\"data\":\"\"}");
    }
}
