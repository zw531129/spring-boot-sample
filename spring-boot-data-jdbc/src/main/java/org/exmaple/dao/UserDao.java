package org.exmaple.dao;

import org.exmaple.model.User;

import java.util.List;

/** Created by zw531 on 2018/1/10. Usage: */
public interface UserDao {
  void saveOneUser(String username, String password);

  void deleteOneUser(Integer id);

  User getOneUser(Integer id);

  List<User> getAllUsers();

  void createTable();
}
