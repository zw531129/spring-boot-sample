# data-collector

## 服务定位
该服务定位为探针服务，从Kafka中接收设备的遥测、告警等业务数据，将数据分类存储至数据仓库并进行数据备份。

## 工程结构
服务为maven经典结构，自定义包采用如下规划：
- bean 存放spring配置Bean类，使用注解模式拒绝繁杂的XML
- consumer 消费者相关, 目前只支持从kafka消费
- framework 整体公共类代码
- vo 值类

## 如何运行
1. 首先安装kafka，参考我写的博客 [Windows 安装 Kafka-2.8.2 单机版](https://my.oschina.net/zw531129/blog/5580686)
2. 使用`mvn clean install`命令打包，产出**data-collector*.zip**
3. `unzip data-collector*.zip`解压zip包，使用`java -jar data-collector.jar`运行服务。 

## 如何调测
1. 首先安装kafka，参考我写的博客 [Windows 安装 Kafka-2.8.2 单机版](https://my.oschina.net/zw531129/blog/5580686)
2. 使用生产者给kafka打点数据，参考步骤1中我的博客有命令
3. 进入Application类运行服务