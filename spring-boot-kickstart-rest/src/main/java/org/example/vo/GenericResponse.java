package org.example.vo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 *
 * @author zw531
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GenericResponse {
    private Integer code;
    private String message;
    private Object data;
}
