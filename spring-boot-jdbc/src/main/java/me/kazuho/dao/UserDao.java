package me.kazuho.dao;

import java.util.List;

import me.kazuho.model.User;

/**
 * Created by zw531 on 2018/1/10. Usage:
 */
public interface UserDao {
  void saveOneUser(String username, String password);

  void deleteOneUser(Integer id);

  User getOneUser(Integer id);

  List<User> getAllUsers();

  void createTable();
}
