package me.kazuho.service;

import me.kazuho.model.User;
import org.springframework.stereotype.Service;


interface UserService {
  User emptyUser();
}
