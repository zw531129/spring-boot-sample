package me;

import org.example.Application;
import org.example.dao.MsgDao;
import org.example.model.Msg;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/** Created by zw531 on 2018/1/11. Usage: */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = Application.class)
public class ApplicationTest {
  @Autowired private MsgDao msgDao;

  @Test
  public void testDao() {
    msgDao.save(new Msg("1", "A1"));
    msgDao.save(new Msg("2", "B2"));
    msgDao.save(new Msg("3", "C3"));

    Assert.assertEquals("A1", msgDao.getOne(1L).getContent());

    msgDao.delete(msgDao.findByTitle("1"));

    // 测试findAll, 查询所有记录, 验证上面的删除是否成功
    Assert.assertEquals(2, msgDao.findAll().size());
  }

  public MsgDao getMsgDao() {
    return msgDao;
  }

  public void setMsgDao(MsgDao msgDao) {
    this.msgDao = msgDao;
  }
}
