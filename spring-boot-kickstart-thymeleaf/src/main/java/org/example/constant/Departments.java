package org.example.constant;

import org.example.model.Department;

import java.util.ArrayList;
import java.util.List;

public enum Departments {
    RESEARCH(1, "Research"),
    DEVELOPMENT(2, "Development"),
    HUMAN_RESOURCE(3, "Human Resource"),
    MARKET(4, "Market");

    private final int did;
    private final String name;

    Departments(int did, String name) {
        this.did = did;
        this.name = name;
    }

    public static List<Department> getAll() {
        List<Department> departments = new ArrayList<>();
        Departments[] values = Departments.values();
        for (Departments each : values) {
            Department dep = new Department();
            dep.setDid(each.getDid());
            dep.setName(each.getName());

            departments.add(dep);
        }

        return departments;
    }

    public static Department getById(Integer deptId) {
        Department dep = new Department();
        for (Departments each : Departments.values()) {
            int did = each.getDid();
            String name = each.getName();
            if (deptId != did) {
                continue;
            }
            dep.setDid(did);
            dep.setName(name);
        }

        return dep;
    }

    public int getDid() {
        return did;
    }

    public String getName() {
        return name;
    }
}
