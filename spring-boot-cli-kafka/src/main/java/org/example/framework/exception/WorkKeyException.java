package org.example.framework.exception;

public class WorkKeyException extends RuntimeException {
    public WorkKeyException(String message, Throwable cause) {
        super(message, cause);
    }

    public WorkKeyException(Throwable cause) {
        super(cause);
    }
}
