# 关于logback的一些tips

### 关于自动扫描

- scan: 当此属性设置为`true`时,配置文件如果发生改变,将会被重新加载,默认值为`true`.
- scanPeriod: 设置监测配置文件是否有修改的时间间隔,如果没有给出时间单位,默认单位是毫秒. 当scan为`true`时,此属性生效。默认的时间间隔为`1分钟`.

### 开启debug模式

- debug: 当此属性设置为`true`时,将打印出logback内部日志信息,实时查看logback运行状态。默认值为`false`.

### 配置结构

- root: root节点是必选节点,用来指定最基础的日志输出级别,只有一个level属性.
    - level: 用来设置打印级别,大小写无关：`TRACE`, `DEBUG`, `INFO`, `WARN`, `ERROR`, `ALL` 和 `OFF`,不能设置为INHERITED或者同义词NULL.

- contextName: 每个logger都关联到logger上下文，默认上下文名称为“default”。但可以使用设置成其他名字，用于区分不同应用程序的记录。一旦设置，不能修改,可以通过 %contextName
  来打印日志上下文名称，一般来说我们不用这个属性，可有可无。

- property: 用来定义变量值的标签， 有两个属性，name和value；其中name的值是变量的名称，value的值时变量定义的值。通过定义的值会被插入到logger上下文中。定义变量后，可以使“${}”来使用变量。

- appender: `appender`用来格式化日志输出节点，有俩个属性name和class，class用来指定哪种输出策略，常用就是控制台输出策略和文件输出策略。

- logger: `logger` 用来设置某 一个包或者具体的某一个类的日志打印级别、以及指定 <appender> 。 <loger> 仅有一个 name 属性，一个可选的 level 和一个可选的 addtivity 属性。