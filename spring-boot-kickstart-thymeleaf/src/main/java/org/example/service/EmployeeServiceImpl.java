package org.example.service;

import org.example.dao.EmployeeRepository;
import org.example.exception.DataProcessException;
import org.example.model.Employee;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    @Autowired
    private EmployeeRepository repository;

    @Override
    public List<Employee> findAll() {
        List<Employee> employees;
        Pageable page = PageRequest.of(0, 10);
        try {
            Page<Employee> pageResult = repository.findAll(page);
            employees = pageResult.getContent();
        } catch (Exception e) {
            throw new DataProcessException("get data failed.", e);
        }

        return employees;
    }

    @Override
    public Employee save(Employee employee) {
        Employee result;
        try {
            result = repository.save(employee);
        } catch (Exception e) {
            throw new DataProcessException("save data failed.", e);
        }

        return result;
    }
}
