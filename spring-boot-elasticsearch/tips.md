# spring-boot-elasticsearch

## elasticsearch

### 操作指南

- 集群管理

```JSON
# 健康状态
GET /_cat/health?v

# 节点列表
GET /_cat/nodes?v




```

- 索引管理

```JSON
# 新增索引(索引名只能小写)
PUT /megacorp
{
  "settings": {
    "index": {
      "number_of_shards": 3,
      "number_of_replicas": 2
    }
  },
  "mappings": {
    # 这里指定的是索引'megacorp'下类型'_doc'的mapping, 6.x及以上版本会删除类型
    # 这一概念, 只保留_doc一种类型. 如果索引下定义了单个类型, 可以对该类型进行更新
    # 不支持在索引下定义多个类型.
    "_doc": {
      "properties": {
        "email": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        }
      }
    }
  }
}

# 删除索引
DELETE /megacorp

# 更新索引
PUT /megacorp/_settings
{
  "number_of_replicas": 1
}

# 查询索引
GET /megacorp

# 关闭/打开索引
POST /megacorp/_close
POST /megacorp/_open

# 增加别名
POST /_aliases
{
  "actions": [
    {
      "add": {
        "index": "megacorp",
        "alias": "view-megacorp",
        # 通过filter指定筛选条件
        "filter": {
          "term": {
            "job": "developer"
          }
        }
      }
    }
  ]
}

# 删除别名
POST /_aliases
{
  "actions": [
    {
      "remove": {
        "index": "megacorp",
        "alias": "view-megacorp"
      }
    }
  ]
}

# 索引统计
GET /megacorp/_stats

# 索引分片
GET /megacorp/_segments

# 索引恢复
GET /megacorp/_recovery?pretty&human

# 合并索引(调用阻塞至合并完成, http请求超时不影响)
POST /megacorp,elasticsearch/_forcemerge

```

### 状态管理

```JSON
# 清除缓存
GET /megacorp/_cache/clear

# 刷新索引
GET /megacorp/_refresh

# flush(ES启发式算法触发, 将数据保存到索引)
GET /megacorp/_flush


```

### 映射管理

```JSON
# 更新映射
PUT megacorp/_mapping/_doc
{
  "properties": {
    # email字段的type不能覆盖, 每个字段只能定义一次
    "email": {
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      }
    },
    "job":{
      "type": "text",
      "fields": {
        "keyword": {
          "type": "keyword",
          "ignore_above": 256
        }
      }
    }
  }
}

# 获取映射
GET /megacorp/_mapping

```

### 模板管理

```JSON
# 新增模板
PUT /_template/mega_template
{
  # 对以maga开头的索引启用该模板
  "index_patterns": [
    "mega*"
  ],
  "settings": {
    "number_of_shards": 5,
    "number_of_replicas": 2
  },
  "mappings": {
    "_doc": {
      "_source": {
        "enabled": true
      },
    # 动态模板, 影响所有新增字段
    "dynamic_templates": [
      {
        "string_fields": {
          "match": "*",
          "match_mapping_type": "string",
          "mapping": {
            "fields": {
              "keyword": {
                "ignore_above": 256,
                "type": "keyword"
              }
            },
            "norms": false,
            "type": "text"
          }
        }
      }
    ]
      "properties": {
        "email": {
          "type": "text",
          "fields": {
            "keyword": {
              "type": "keyword",
              "ignore_above": 256
            }
          }
        }
      }
    }
  }
}

# 删除模板
DELETE /_template/mega_template

# 查看模板
GET /_template/mega_template

```

### 文档管理

```JSON
# 增加/更新文档
PUT /megacorp/_doc/1
{
  "email": "*@*.com"
}

# 删除文档
DELETE /megacorp/_doc/1

# 获取文档

GET /megacorp/_doc/1

# 批量查询
POST /_mget?pretty
{
  "docs": [
    {
      "_index": "megacorp",
      "_type": "_doc",
      "_id": "1"
    },
    {
      "_index": "megacorp",
      "_type": "_doc",
      "_id": "2"
    }
  ]
}

# bulk-api使用
POST /_bulk
{"index":{"_index":"megacorp","_type":"_doc","_id":"1"}}
{"email":"*@huawei.com"}
{"delete":{"_index":"megacorp","_type":"_doc","_id":"2"}}
{"create":{"_index":"megacorp","_type":"_doc","_id":"3"}}
{"email":"*@gmail.com"}
{"update":{"_id":"1","_type":"_doc","_index":"megacorp"}}
{"doc":{"email":"*@outlook.com"}}


```

### 搜索

```JSON
# 全文检索


# DSL查询
GET /megacorp/_search
{
  "size": 10,
  "from": 0,
  "query": {
    "bool": {
      "must": [
        {
          "term": {
            "lastName.keyword": {
              "value": "Zhang"
            }
          }
        }
      ],
      "filter": {
        "terms": {
          "email": [
            "*@gmail.com",
            "*@outlook.com"
          ]
        },
        "range": {
          "joinDate": {
            "gte": "2018-01-01",
            "lte": "2019-01-01"
          }
        }
      }
    }
  }
}

### 全文检索
GET /megacorp/_search
{
  "query": {
    "match": {
      "interest": "Swimming, Programming"
    }
  }
}

```

### 聚合
```JSON
### 度量聚合
GET /megacorp/_search
{
  # 查询语句
  "aggs": {
    "avg_grade": {
      # elasticsearch内置聚合type, 还有max/min等其他类型
      "avg": {
        "field": "grade"
      }
    }
  }
}

### 分组聚合
GET /megacorp/_search
{
  "size": 0,
  "aggs": {
    "group_by_tags": {
      "terms": {
        "field": "tags"
      },
      "aggs": {
        "avg_price": {
          # 使用自带的type
          "avg": {
            "field": "price"
          }
        }
      }
    }
  }
}

GET /megacorp/_search
{
  "size": 0,
  "aggs": {
    "categoryGroup": {
      "terms": {
        "field": "category"
      },
      "aggs": {
        "brandGroup": {
          # 自定义聚合类型
          "terms": {
            "field": "brand"
          }
        }
      }
    }
  }
}

```


### 高版本迁移

- 6.x版本没有`string`类型, 替代类型为`keyword`与`text`.

- 6.x以上版本会删除`index`的`type`属性

### 自定义配置

- `text`相当于`analyzed`的`string`, `keyword`相当于`not_analyzed`的`string`, 可以通过`index`属性设置true/false指定是否进行分析.

- `text`与`fields`及`keyword`组合可以达到同时支持全文检索与精确匹配的效果.

## spring-data-elasticsearch