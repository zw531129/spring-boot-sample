package me.kazuho.util;

import java.util.Date;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * 静态方法测试类
 */
@RunWith(SpringJUnit4ClassRunner.class)
public class TimeUtilTest {

  @Test
  public void testToday(){
    Assert.assertEquals(new Date().toString(), TimeUtil.today().toString());
  }
}
