package me.kazuho.service;

import java.util.List;

import me.kazuho.model.User;

public interface UserService {
  User create(User user);

  User findById(Integer id);

  User update(User user);

  User delete(Integer id);

  List<User> findAll();
}
