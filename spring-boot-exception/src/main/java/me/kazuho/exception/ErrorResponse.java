package me.kazuho.exception;

import java.util.Objects;

public class ErrorResponse<T> {
  private Integer errorCode;

  private String msg;

  private T data;

  @Override
  public String toString() {
    return "ErrorResponse{" +
            "errorCode=" + errorCode +
            ", msg='" + msg + '\'' +
            ", data=" + data +
            '}';
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    ErrorResponse<?> that = (ErrorResponse<?>) o;
    return Objects.equals(errorCode, that.errorCode) &&
            Objects.equals(msg, that.msg) &&
            Objects.equals(data, that.data);
  }

  @Override
  public int hashCode() {
    return Objects.hash(errorCode, msg, data);
  }

  public Integer getErrorCode() {
    return errorCode;
  }

  public void setErrorCode(Integer errorCode) {
    this.errorCode = errorCode;
  }

  public String getMsg() {
    return msg;
  }

  public void setMsg(String msg) {
    this.msg = msg;
  }

  public T getData() {
    return data;
  }

  public void setData(T data) {
    this.data = data;
  }
}
