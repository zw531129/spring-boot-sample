package org.example.exception.permission;

import org.example.exception.BaseException;

public class UserListException extends BaseException {
    public UserListException() {
    }

    public UserListException(String message) {
        super(message);
    }

    public UserListException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserListException(Throwable cause) {
        super(cause);
    }

    public UserListException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
