package org.example.model

/**
 *
 * @author zhangyf2595
 * @date 2024/12/2
 * @version 1.0
 * */

class Person {
    String name
    Long age
}
