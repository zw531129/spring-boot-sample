package org.example.exception.permission;

import org.example.exception.BaseException;

public class UserAddException extends BaseException {
    public UserAddException() {
    }

    public UserAddException(String message) {
        super(message);
    }

    public UserAddException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserAddException(Throwable cause) {
        super(cause);
    }

    public UserAddException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
