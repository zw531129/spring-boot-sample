package org.example.advice;

import org.example.constant.UserErrorCode;
import org.example.exception.permission.*;
import org.example.vo.GenericResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collections;
import java.util.Locale;

@RestControllerAdvice
@Slf4j
public class TestCaseErrorHandler {
    @Autowired
    private MessageSource messageSource;
    @ExceptionHandler(UserAddException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GenericResponse addFail() {
        Integer code = UserErrorCode.ADD_FAIL.code;
        String errMsg = messageSource.getMessage(UserErrorCode.ADD_FAIL.message, null, Locale.CHINA);
        GenericResponse gr = GenericResponse.builder()
                .code(code)
                .message(errMsg)
                .data(Collections.emptyMap())
                .build();

        return gr;
    }

    @ExceptionHandler(UserDeleteException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GenericResponse deleteFail() {
        Integer code = UserErrorCode.DELETE_FAIL.code;
        String errMsg = messageSource.getMessage(UserErrorCode.DELETE_FAIL.message, null, Locale.CHINA);
        GenericResponse gr = GenericResponse.builder()
                .code(code)
                .message(errMsg)
                .data(Collections.emptyMap())
                .build();

        return gr;
    }

    @ExceptionHandler(UserDetailException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GenericResponse getDetailFail() {
        Integer code = UserErrorCode.GET_DETAIL_FAIL.code;
        String errMsg = messageSource.getMessage(UserErrorCode.GET_DETAIL_FAIL.message, null, Locale.CHINA);
        GenericResponse gr = GenericResponse.builder()
                .code(code)
                .message(errMsg)
                .data(Collections.emptyMap())
                .build();

        return gr;
    }

    @ExceptionHandler(UserUpdateException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GenericResponse updateFail() {
        Integer code = UserErrorCode.UPDATE_FAIL.code;
        String errMsg = messageSource.getMessage(UserErrorCode.UPDATE_FAIL.message, null, Locale.CHINA);
        GenericResponse gr = GenericResponse.builder()
                .code(code)
                .message(errMsg)
                .data(Collections.emptyMap())
                .build();

        return gr;
    }

    @ExceptionHandler(UserListException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public GenericResponse listFail() {
        Integer code = UserErrorCode.LIST_FAIL.code;
        String errMsg = messageSource.getMessage(UserErrorCode.LIST_FAIL.message, null, Locale.CHINA);
        GenericResponse gr = GenericResponse.builder()
                .code(code)
                .message(errMsg)
                .data(Collections.emptyMap())
                .build();

        return gr;
    }

}
