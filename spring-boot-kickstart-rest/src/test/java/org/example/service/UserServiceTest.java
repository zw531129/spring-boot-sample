//package org.example.service;
//
//import org.example.dao.UserDaoImpl;
//import org.example.model.User;
//import org.junit.Assert;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.mockito.InjectMocks;
//import org.mockito.Mock;
//import org.mockito.Mockito;
//import org.mockito.junit.MockitoJUnitRunner;
//
//@RunWith(MockitoJUnitRunner.class)
//public class UserServiceTest {
//
//  @Mock private UserDaoImpl userDao;
//
//  @InjectMocks private UserServiceImpl userService;
//
//  @Test
//  public void testGetEmptyUser() {
//    //  构造要返回的假数据
//    User user = new User();
//    //  mock掉返回的数据
//    Mockito.when(userDao.getEmptyUser()).thenReturn(user);
//    User emptyUser = userService.emptyUser();
//    //  判断是否相等(只能比String, Object场景下Expected会多一个换行出来)
//    Assert.assertEquals(new User().toString(), emptyUser.toString());
//  }
//}
