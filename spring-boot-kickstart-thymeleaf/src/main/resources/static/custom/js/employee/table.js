    //  获取主机的协议/主机名/端口号
    var protocol = window.location.protocol;
    var host = window.location.host;
    var port = window.location.port;

    var table = $('employee-table').DataTable({
        /*  隐藏buttons
         *  筛选器左上
         *  搜索右上
         *  计数器左下
         *  分页右下
         */
        sDom: ' <"top pull-left invisible"B> <"top pull-left"l> <"top pull-right"f> <"bottom pull-left"i> <"bottom pull-right"p>',
        buttons:[
            {
                extend: 'excel',
                filename: 'something',
                title: null,
                exportOptions: []
            }
        ],
        //  每页默认显示
        pageLength: 25,
        //  延迟渲染
        deferRender: true,
        //  X轴滚动
        scrollX: true,
        //  自动列宽
        autoWidth: true,
        //  本地化
        language:{
            //  自定义筛选器
            lengthMenu: "",
            search: "",
            searchPlaceholder: "Searching...",
            paginate:{
                previous: "上一页",
                next: "下一页",
                first: "最前",
                last: "最后"
            },
            //  无内容时tbody显示
            zeroRecords: "zeroRecords",
            info: "共 _PAGES_ 页, 显示第 _START_ 条至第 _END_ 条, 共 _MAX_ 条.",
            infoEmpty: "共 0 条记录",
            infoFiltered: ""
        },
        //  设置每一列
        columnDefs:[
            {width: "100px", targets: [0, 1]}
        ]
    })

    /**
     *  全选, 反选功能
     */
    //  指定第0列的tr为checkbox形式后操作这个checkbox
    $("#select_all").click(function(){
        if(this.checked){
            $(".checkbox_select").each(function () {
                this.checked = true;
            });
            //  为所有选中的行添加selected样式
            $(":input[name='ids']").addClass("selected");
        }else{
            $(".checkbox_select").each(function(){
                this.checked = false;
            });

            //  反选删除selected样式
            $(":input[name='ids']").removeClass("selected");
        }
    });

    //  单选某一行, 添加selected样式
    $('#employee-table tbody').on( 'click', 'input', function () {
        $(this).toggleClass('selected');
    } );

    //
    $("#delete").click(function () {
        var ids = [];
        var verId = $('#verId').val()

        //  获取.selected元素的value
        $(".selected").each(function(){
            ids.push($(this).val().trim());
        });

        //  没有选择到ID则不发送
        if(ids.length === 0){
            return ;
        }

        //  数组转成JSON
        ids = JSON.stringify(ids);

        $.ajax({
            url: protocol + '//' + host + '/version/' + verId + '/delete/diff',
            //  跨域请求只能用get
            type: "post",
            data: {
                "ids": ids,
                "id": verId
            },
            success: function(data){
                //  ajax请求不会刷新页面, 在这里手动刷新一次
                window.location.href = protocol + '//' + host + data.url
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                //  失败打印错误码, 维测信息
                console.log("Delete objects failed! XMLHttpRequest.status: "
                            + XMLHttpRequest.status + " XMLHttpRequest.readyState: "
                            + XMLHttpRequest.readyState + " textStatus: "
                            + textStatus
                )
            }
        });
    });

    $("#update").click(function () {
        var ids = [];
        var id;

        //  获取.selected元素的value
        $(".selected").each(function(){
            ids.push($(this).val().trim());
        });

        //  没有选择到ID则不发送
        if(ids.length === 0){
            return ;
        }

        //  多个Id不发送
        if(ids.length !== 1){
            return ;
        }

        //  数字转字符串
        id = parseInt(ids[0], 0);
        //  发送url
        var url;

        //  url = protocol + '//' + host + '';

        //  更新地址栏, 跳转到update页面
        window.location.href = url;
    });


    $("#add").click(function () {
        var verId = $('#verId').val()
        //  获取协议, 做成动态
        //  var url = protocol + '//' + host + '';
        //  更新地址栏, 跳转到update页面
        window.location.href = url;
    });

    //  先把datatable自己的button干掉
    $(document).ready(function() {
        $('.buttons-excel').css({
            'display' : 'none'
        })
    });

    //  导出excel
    $('#export').click(function(){
        $('.buttons-excel').click()
    })