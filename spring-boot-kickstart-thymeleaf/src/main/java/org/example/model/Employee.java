package org.example.model;

import org.example.validation.ValidateTip;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by zw531 on 2018/4/9. Usage:
 */
@Entity
@Table(name = "employee")
public class Employee {
    @GeneratedValue
    @Id
    @Column(name = "id")
    private Integer id;

    @NotEmpty(message = ValidateTip.NOT_EMPTY)
    @Column(name = "first_name")
    private String firstName;

    @NotEmpty(message = ValidateTip.NOT_EMPTY)
    @Column(name = "last_name")
    private String lastName;

    @NotNull(message = ValidateTip.NOT_NULL)
    @Column(name = "sn")
    private String SN;
    // 地址 员工 一一对应, 员工没了地址也没用了
    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Address address;
    // 部门不会随着员工没了消失, 只加存储级联操作
    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Department department;
    // 员工可以增加新岗位
    @NotEmpty(message = ValidateTip.NOT_EMPTY)
    @ManyToMany(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Set<Role> roles = new HashSet<>();

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @Column(name = "joined_time")
    private LocalDate joinedTime;

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", SN='" + SN + '\'' +
                ", address=" + address +
                ", department=" + department +
                ", roles=" + roles +
                ", joinedTime=" + joinedTime +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getSN() {
        return SN;
    }

    public void setSN(String SN) {
        this.SN = SN;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Set<Role> getRoles() {
        return roles;
    }

    public void setRoles(Set<Role> roles) {
        this.roles = roles;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public LocalDate getJoinedTime() {
        return joinedTime;
    }

    public void setJoinedTime(LocalDate joinedTime) {
        this.joinedTime = joinedTime;
    }
}
