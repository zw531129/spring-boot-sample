package org.example.exception.permission;

import org.example.exception.BaseException;

public class UserDetailException extends BaseException {
    public UserDetailException() {
    }

    public UserDetailException(String message) {
        super(message);
    }

    public UserDetailException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserDetailException(Throwable cause) {
        super(cause);
    }

    public UserDetailException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
