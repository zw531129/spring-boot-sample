package org.example.vo;

import org.example.model.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;

@Data
@Validated
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {
    private String uid;
    @NotBlank(message = "{validation.user.serial.blank}")
    private String username;
    @NotBlank(message = "{validation.user.name.blank}")
    private String password;
    @NotBlank(message = "{validation.user.script.blank}")
    private String nickname;
    private Long createTime;
    private Long updateTime;

    public UserDto(User user) {
        this.uid = user.getUid();
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.nickname = user.getNickname();
        this.createTime = user.getCreateTime();
        this.updateTime = user.getUpdateTime();
    }
}
