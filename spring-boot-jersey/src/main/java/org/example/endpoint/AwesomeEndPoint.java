package org.example.endpoint;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Component
@Path("/")
public class AwesomeEndPoint {
    @GET
    @Path("/hello")
    @Produces("application/json")
    public ResponseEntity<?> greeting() {
        return new ResponseEntity<>("greeting!", HttpStatus.OK);
    }
}
