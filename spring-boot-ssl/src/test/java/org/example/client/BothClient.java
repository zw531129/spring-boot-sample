package org.example.client;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import javax.net.ssl.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.KeyStore;
import java.security.SecureRandom;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;

public class BothClient {
    //安全传输层协议
    private static final String PROTOCOL = "TLS";

    // JKS/PKCS12
    private static final String KEY_KEYSTORE_TYPE = "PKCS12";

    public static SSLSocketFactory getSocketFactory(String cerPath, String p12Path, String password) throws Exception {
        InputStream cerInputStream = null;
        InputStream p12InputStream = null;
        SSLSocketFactory socketFactory = null;
        try {
            cerInputStream = new FileInputStream(new File(cerPath));
            p12InputStream = new FileInputStream(new File(p12Path));
            KeyManager[] keyManagers = getKeyManagers(p12InputStream, password);
            TrustManager[] trustManagers = getTrustManagers(cerInputStream);
            SSLContext sslContext = getSslContext(keyManagers, trustManagers);
            socketFactory = sslContext.getSocketFactory();
        } finally {
            if (cerInputStream != null) {
                cerInputStream.close();
            }
            if (p12InputStream != null) {
                p12InputStream.close();
            }
        }
        return socketFactory;
    }

    private static SSLContext getSslContext(KeyManager[] keyManagers, TrustManager[] trustManagers) throws Exception {
        SSLContext sslContext = SSLContext.getInstance(PROTOCOL);
        sslContext.init(keyManagers, trustManagers, new SecureRandom());
        return sslContext;
    }

    private static KeyManager[] getKeyManagers(InputStream inputStream, String password) throws Exception {
        KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
        KeyStore keyStore = KeyStore.getInstance(KEY_KEYSTORE_TYPE);
        //加载证书
        keyStore.load(inputStream, password.toCharArray());
        keyManagerFactory.init(keyStore, password.toCharArray());
        KeyManager[] keyManagers = keyManagerFactory.getKeyManagers();
        return keyManagers;
    }

    private static TrustManager[] getTrustManagers(InputStream inputStream) throws Exception {
        TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
        KeyStore keyStore = KeyStore.getInstance(KEY_KEYSTORE_TYPE);
        //加载证书
        CertificateFactory certificateFactory = CertificateFactory.getInstance("X.509");
        Certificate ca = certificateFactory.generateCertificate(inputStream);
        keyStore.load(null, null);
        //设置公钥
        keyStore.setCertificateEntry("server", ca);
        trustManagerFactory.init(keyStore);
        TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
        return trustManagers;
    }

    public static void main(String[] args) throws Exception {
        //获取SSLSocketFactory
        String certPath = "C:\\Users\\zw531\\Workspace\\spring-boot-sample\\spring-boot-ssl\\src\\main\\resources\\cert\\both\\server.cer";//服务端公钥
        String p12Path = "C:\\Users\\zw531\\Workspace\\spring-boot-sample\\spring-boot-ssl\\src\\main\\resources\\cert\\both\\client.p12";//客户端私钥
        //发送请求
        String url = "https://127.0.0.1:8080/trustAll";
        OkHttpClient.Builder clientBuilder = new OkHttpClient.Builder();
        SSLSocketFactory socketFactory = getSocketFactory(certPath, p12Path, "123456");
        TrustManager[] trustManagers = getTrustManagers(Files.newInputStream(Paths.get(certPath)));
        clientBuilder.sslSocketFactory(socketFactory, (X509TrustManager) trustManagers[0]);
        //解决报错javax.net.ssl.SSLPeerUnverifiedException: Hostname 127.0.0.1 not verified
        clientBuilder.hostnameVerifier(new HostnameVerifier() {
            @Override
            public boolean verify(String s, SSLSession sslSession) {
                System.out.println("主机:" + s);
                return true;
            }
        });
        OkHttpClient client = clientBuilder.build();

        Request.Builder builder = new Request.Builder().url(url);
        Request request = builder.build();

        Response response = client.newCall(request).execute();
        String result = response.body().string();
        //打印结果返回数据
        System.out.println(result);
    }
}
