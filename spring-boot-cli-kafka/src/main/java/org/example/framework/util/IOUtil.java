package org.example.framework.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Objects;

public final class IOUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(IOUtil.class);

    private IOUtil() {
    }

    /**
     * 获取jar包根路径
     *
     * @return jar包根路径
     */
    public static String getRootPath() {
        String basePath = Objects.requireNonNull(IOUtil.class.getResource("/")).getPath();
        String osType = System.getProperty("os.name");

        if (osType.startsWith("Windows")) {
            basePath = basePath.substring(1);
            return basePath.substring(0, basePath.lastIndexOf('/'));
        }

        return basePath.substring(0, basePath.lastIndexOf('/'));
    }
}
