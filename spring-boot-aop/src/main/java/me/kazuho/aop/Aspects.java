package me.kazuho.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by zw531 on 2018/1/13.
 * 拦截顺序按照@Order中指定的数值来定, 越小越靠前.
 */
@Aspect
@Component
public class Aspects {
  private Logger logger = LoggerFactory.getLogger(Aspects.class);

  private ThreadLocal<Long> startTime = new ThreadLocal<Long>();

  @Pointcut("execution(public * me.kazuho.web..*.*(..))")
  public void controllerLogFlow() {

  }

  @Before("controllerLogFlow()")
  @Order(3)
  public void doBefore(JoinPoint joinPoint) {
    //  set thread level var
    startTime.set(System.currentTimeMillis());

    //  Get http request
    ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
    HttpServletRequest request = attributes.getRequest();

    //  Log something
    logger.info("URL: " + request.getRequestURL().toString());
    logger.info("HTTP METHOD: " + request.getMethod());
    logger.info("IP: " + request.getRemoteAddr());
    logger.info("CLASS METHOD: " + joinPoint.getSignature().getDeclaringTypeName()
            + "." + joinPoint.getSignature().getName());
    logger.info("ARGS : " + Arrays.toString(joinPoint.getArgs()));
  }

  @AfterReturning(returning = "ret", pointcut = "controllerLogFlow()")
  @Order(5)
  public void doAfterReturning(Object ret) throws Throwable {
    logger.info("RESPONSE : " + ret);
  }

  @After("controllerLogFlow()")
  @Order(7)
  public void doAfter() {
    logger.info("SPEED TIME: " + (System.currentTimeMillis() - startTime.get()));
  }

}
