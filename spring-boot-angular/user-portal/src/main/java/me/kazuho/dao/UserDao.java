package me.kazuho.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import me.kazuho.model.User;

public interface UserDao extends JpaRepository<User, Integer> {

}
