package me.kazuho.util;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class TimeUtil {

  public static Date today(){
    LocalDateTime ldt = LocalDateTime.now();
    Instant now = ldt.atZone(ZoneId.systemDefault()).toInstant();

    return Date.from(now);
  }

}
