package org.example.service;

import org.example.model.Msg;

import java.util.List;

/** Created by zw531 on 2018/1/4. Usage: */
public interface MsgService {

  void addMsg(Msg msg);

  List<Msg> getAllMsgs();

  void deleteMsg(Long id);

  Msg findByTitle(String title);
}
