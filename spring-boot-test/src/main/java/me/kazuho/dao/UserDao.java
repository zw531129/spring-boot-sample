package me.kazuho.dao;

import me.kazuho.model.User;

public interface UserDao {
  User getEmptyUser();
}
