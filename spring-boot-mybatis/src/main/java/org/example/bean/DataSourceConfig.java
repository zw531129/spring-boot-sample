//package org.example.bean;
//
//import com.zaxxer.hikari.HikariConfig;
//import com.zaxxer.hikari.HikariDataSource;
//import org.apache.ibatis.session.SqlSessionFactory;
//import org.mybatis.spring.SqlSessionFactoryBean;
//import org.mybatis.spring.SqlSessionTemplate;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.env.Environment;
//import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
//import org.springframework.jdbc.datasource.DataSourceTransactionManager;
//import org.springframework.transaction.PlatformTransactionManager;
//
//import javax.sql.DataSource;
//import java.io.IOException;
//
///**
// * 手动配置Mybatis数据源
// */
//@Configuration
//public class DataSourceConfig {
//    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourceConfig.class);
//    private static final String DRIVER_CLASS = "monitor.datasource.driver-class-name";
//    private static final String JDBC_URL = "monitor.datasource.url";
//    private static final String USERNAME = "monitor.datasource.ak";
//    private static final String PASSWORD = "monitor.datasource.sk";
//    @Autowired
//    private Environment env;
//
//    @Autowired
//    private SecurityManager securityManager;
//
//    /**
//     * 初始化数据源
//     *
//     * @return dataSource
//     */
//    @Bean
//    public DataSource dataSource() {
//        // JDBC配置
//        String driverClass = env.getProperty(DRIVER_CLASS);
//        String jdbcUrl = env.getProperty(JDBC_URL);
//        String user = env.getProperty(USERNAME);
//        String password = env.getProperty(PASSWORD);
//        // HikariCP连接池配置
//        HikariConfig hikariConfig = new HikariConfig();
//        hikariConfig.setDriverClassName(driverClass);
//        hikariConfig.setJdbcUrl(jdbcUrl);
//        hikariConfig.setUsername(user);
//        hikariConfig.setPassword(password);
//        LOGGER.info("build connectorPool complete");
//
//        return new HikariDataSource(hikariConfig);
//    }
//
//    /**
//     * 初始化SqlSessionFactory
//     *
//     * @return sqlSessionFactory
//     * @throws IOException e
//     */
//    @Bean
//    public SqlSessionFactoryBean sqlSessionFactory() throws IOException {
//        SqlSessionFactoryBean sqlSessionFactory = new SqlSessionFactoryBean();
//        sqlSessionFactory.setDataSource(dataSource());
//        try {
//            sqlSessionFactory.setMapperLocations(
//                    new PathMatchingResourcePatternResolver().getResources("classpath:mapper/*.xml"));
//        } catch (IOException e) {
//            LOGGER.error("unable to find mybatis mapper in resources dir.");
//            throw new IOException(e);
//        }
//        // 开启驼峰转换, mybatis默认不会把数据库下划线转换成驼峰
//        org.apache.ibatis.session.Configuration config = new org.apache.ibatis.session.Configuration();
//        config.setMapUnderscoreToCamelCase(true);
//        sqlSessionFactory.setConfiguration(config);
//
//        return sqlSessionFactory;
//    }
//
//    /**
//     * 初始化SqlSessionTemplate
//     *
//     * @param sqlSessionFactory SqlSessionFactory
//     * @return sqlSessionFactory
//     */
//    @Bean
//    public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) {
//        return new SqlSessionTemplate(sqlSessionFactory);
//    }
//
//    /**
//     * 初始化PlatformTransactionManager
//     *
//     * @return transactionManager
//     */
//    @Bean
//    public PlatformTransactionManager transactionManager() {
//        return new DataSourceTransactionManager(dataSource());
//    }
//}
