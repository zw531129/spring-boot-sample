package me.kazuho.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import me.kazuho.model.User;

/**
 * Created by zw531 on 2017/12/25. Usage:
 */
public interface SysUserDao extends JpaRepository<User, Integer> {
  User findByUsername(String username);
}
