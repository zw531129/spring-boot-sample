package org.example.exception;

public class DataSourceInitFailedException extends RuntimeException {
    public DataSourceInitFailedException(String s, Exception e) {
        super(s, e);
    }
}
