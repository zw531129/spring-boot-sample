//package org.example.producer;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.kafka.core.KafkaTemplate;
//import org.springframework.stereotype.Component;
//
///**
// * @author zhangyf2595
// * @version 1.0
// * @date 2023/8/18
// **/
//@Component
//public class SpringKafkaProducer {
//    @Autowired
//    private KafkaTemplate<String, String> kafkaTemplate;
//
//    public void send(String msg) {
//        kafkaTemplate.send("foobar", msg);
//    }
//}
