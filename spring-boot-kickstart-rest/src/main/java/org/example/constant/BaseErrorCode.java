package org.example.constant;


public enum BaseErrorCode {
    NOT_FOUND(404, "base.error.not_found"),
    INTERNAL_ERROR(500, "base.error.internal"),
    BAD_REQUEST(400, "base.error.bad_request"),
    INVOKE_SUCCESS(20000, "base.success.invoke"),
    INVOKE_FAIL(50000, "base.error.invoke");

    public final Integer code;
    public final String message;
    BaseErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
