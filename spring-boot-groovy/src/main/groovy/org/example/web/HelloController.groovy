package org.example.web

import groovy.util.logging.Slf4j
import org.example.model.Person
import org.springframework.web.bind.annotation.*

/**
 *
 * @author zhangyf2595
 * @date 2024/12/2
 * @version 1.0
 * */
@Slf4j
@RestController
class HelloController {
    @GetMapping("/greeting")
    String greeting() {
        return "Hello Groovy!"
    }

    @GetMapping("/str")
    String gString(@RequestParam("name") String name) {
        String str = "Hello ${name}, you are now using GString"
        String sqStr = 'an escaped single quote: \' needs a backslash'
        log.info("this is single quoted string ${sqStr}")
        String sum = "sum of 2 and 3 is ${2 + 3}"
        log.info("this is single quoted string ${sum}")

        return str
    }

    @PostMapping("/map")
    Map<Object, Object> map(@RequestBody Person p) {
        log.info("receive person object from ${p}")
        p.name = "张献忠"
        p.age = 5000
        HashMap<Object, Object> ret = [:]
        ret["name"] = p.name
        ret["age"] = p.age
        return ret
    }

    @PostMapping("/list")
    List<Object> list(@RequestBody List<Integer> arr) {
        List<Object> ret = arr.collect {
            it * 2
        }.toList()
        arr.last()

        return ret
    }

}
