# 生成客户端证书
keytool -genkey -alias client -keypass 123456 -keyalg RSA -keysize 1024 -validity 365 -storepass 123456 -storetype PKCS12 -keystore client.p12
# 导出客户端cer证书
keytool -export -alias client -keystore client.p12 -storetype PKCS12 -keypass 123456 -file client.cer
# 生成keystore用来存储springboot信任的证书
keytool -genkey -alias springboot_keystore -keypass 123456 -keyalg RSA -keysize 1024 -validity 365 -storepass 123456 -storetype PKCS12 -keystore springboot_keystore.keystore
# 4导入客户端的公钥到springboot_keystore.keystore
keytool -import -v -file client.cer -keystore springboot_keystore.keystore -storepass 123456