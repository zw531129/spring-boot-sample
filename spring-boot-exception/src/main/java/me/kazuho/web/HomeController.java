package me.kazuho.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import me.kazuho.exception.ErrorResponse;
import me.kazuho.service.HomeService;

/**
 * Created by zw531 on 2018/2/7. Usage:
 */
@Controller
public class HomeController {

  @Autowired
  private HomeService homeService;

  @RequestMapping("/ui/home")
  public String uiHome() {

    homeService.hi();

    return "home/index";
  }

  @RequestMapping("/rest/home")
  public ResponseEntity<?> restHome() {

    boolean ret = homeService.greeting();

    if (!ret) {
      ErrorResponse<String> response = new ErrorResponse<>();
      response.setErrorCode(1000080);
      response.setMsg("Exception rise due to zero dive.");
      response.setData("Your response VO here.");

      return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    return new ResponseEntity<>("Request succeed!", HttpStatus.OK);
  }

  public HomeService getHomeService() {
    return homeService;
  }

  public void setHomeService(HomeService homeService) {
    this.homeService = homeService;
  }
}
