//package org.example.web;
//
//import org.example.model.User;
//import org.example.service.UserService;
//import org.junit.Test;
//import org.junit.runner.RunWith;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
//import org.springframework.boot.test.mock.mockito.MockBean;
//import org.springframework.http.MediaType;
//import org.springframework.test.context.junit4.SpringRunner;
//import org.springframework.test.web.servlet.MockMvc;
//import org.springframework.test.web.servlet.RequestBuilder;
//import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
//
//import static org.hamcrest.Matchers.equalTo;
//import static org.junit.Assert.assertNotNull;
//import static org.mockito.ArgumentMatchers.any;
//import static org.mockito.Mockito.when;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
///**
// * Mock单元测试
// */
//@RunWith(SpringRunner.class)
//@WebMvcTest(UserController.class)
//public class UserControllerTest {
//  @MockBean
//  private UserService userService;
//  @Autowired
//  private MockMvc mockMvc;
//
//  @Test
//  public void testGreet() throws Exception {
//    RequestBuilder request = null;
//
//    //  test method get()
//    request = MockMvcRequestBuilders.get("/greet/Ivan")
//                                    .header("X-Sec-Key", "foobar")
//                                    .param("greeting", "Hello");
//    mockMvc.perform(request)
//            .andExpect(content().string(equalTo("{\"code\":20000,\"message\":\"Hello Ivan\",\"data\":\"\"}")));
//
//    // test method update()
//    // request = put("/user/1/update").param("name", "Geralt").param("age", "100");
//    // homeControllerMock.perform(request).andExpect(content().string(equalTo("update user: 1 success")));
//
//    // test method delete()
//    // request = delete("/user/1/delete");
//    // homeControllerMock.perform(request).andExpect(content().string(equalTo("remove user: 1 success")));
//  }
//
//  @Test
//  public void testAdd() throws Exception {
//    RequestBuilder request = null;
//    //  test method post()
//    request = MockMvcRequestBuilders.post("/add")
//                                    .contentType(MediaType.APPLICATION_JSON)
//                                    .header("X-Sec-Key", "")
//                                    .content("{\"username\":\"tiger\",\"description\":\"woods\"}");
//    when(userService.save(any())).thenReturn(new User());
//    mockMvc.perform(request).andExpect(status().isOk())
//                            .andExpect(content().string(equalTo("{\"code\":20000,\"message\":\"new user added!\",\"data\":{\"id\":null,\"uid\":null,\"username\":\"tiger\",\"description\":\"woods\",\"joinedTime\":null}}")));
//  }
//
//
////  @Test
////  public void testReadingList2() throws Exception{
////    assertNotNull(mockMvc);
////    when(repository.findBooksByReader(any())).thenReturn(Arrays.asList(
////            new Book(1l, "yulaoba", "123123123123123", "JAVA", "Smith", "asdfafd")
////    ));
////    mockMvc.perform(get("/readinglist/api/{reader}", "yulaoba"))
////            .andExpect(status().isOk())
////            .andExpect(content().contentType(MediaType.APPLICATION_JSON))
////            .andExpect(content().json("[{'id': 1, 'reader': 'yulaoba', 'isbn': '123123123123123', 'title': 'JAVA', 'author': 'Smith', 'description': 'asdfafd'}]"))
////            .andExpect(jsonPath("$[0]['title']").value("JAVA"));
////  }
//}
