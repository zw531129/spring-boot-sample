CREATE TABLE `tbl_user` (
  `id` bigint unsigned NOT NULL PRIMARY KEY AUTO_INCREMENT COMMENT '自增ID',
  `uid` varchar(255) NOT NULL COMMENT '用户ID',
  `username` varchar(255) NOT NULL COMMENT '用户名',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `nickname` text NOT NULL COMMENT '昵称',
  `create_time` bigint  NOT NULL COMMENT '创建时间',
  `update_time` bigint NOT NULL COMMENT '更新时间'
);