package me.kazuho.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import me.kazuho.dao.SysUserDao;
import me.kazuho.model.Role;
import me.kazuho.model.User;

/**
 * Created by zw531 on 2017/12/25. Usage:
 */
public class UserServiceImpl implements UserService {
  @Autowired
  private SysUserDao sysUserDao;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    User user = sysUserDao.findByUsername(username);
    if (user == null) {
      throw new UsernameNotFoundException("用户名不存在");
    }

    System.out.println("------- UserServiceImpl -------");
    System.out.println("user.username: " + user.getUsername());
    System.out.println("user.password: " + user.getPassword());
    for (Role role : user.getRoles()) {
      System.out.println("user.role: " + role.getName());
    }
    System.out.println("------- UserServiceImpl -------");

    return user;
  }

  public SysUserDao getSysUserDao() {
    return sysUserDao;
  }

  public void setSysUserDao(SysUserDao sysUserDao) {
    this.sysUserDao = sysUserDao;
  }
}
