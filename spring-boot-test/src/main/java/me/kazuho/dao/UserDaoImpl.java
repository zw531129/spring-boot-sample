package me.kazuho.dao;

import me.kazuho.model.User;
import org.springframework.stereotype.Repository;

@Repository
public class UserDaoImpl implements UserDao {

  @Override
  public User getEmptyUser() {
    return null;
  }
}
