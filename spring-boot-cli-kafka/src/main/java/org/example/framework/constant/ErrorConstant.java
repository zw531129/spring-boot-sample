package org.example.framework.constant;

public enum ErrorConstant {
    SUCCESS(20000, "SUCCESS"),
    FAIL(50000, "FAIL");

    private final int code;
    private final String message;

    ErrorConstant(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
