package org.example.framework.exception;

public class RootKeyException extends RuntimeException {
    public RootKeyException(String message, Throwable cause) {
        super(message, cause);
    }

    public RootKeyException(Throwable cause) {
        super(cause);
    }
}
