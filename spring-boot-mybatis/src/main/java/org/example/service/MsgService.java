package org.example.service;

import org.example.model.Msg;

import java.util.List;

/** Created by zw531 on 2018/1/11. Usage: */
public interface MsgService {
  List<Msg> getAllMsgs();

  Msg findById(Long id);

  Msg findByTitle(String title);

  void delete(Long id);

  void add(Msg msg);

  void update(Msg msg);

  void createTable(String tableName);
}
