package org.example.exception.permission;

import org.example.exception.BaseException;

public class UserDeleteException extends BaseException {
    public UserDeleteException() {
    }

    public UserDeleteException(String message) {
        super(message);
    }

    public UserDeleteException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserDeleteException(Throwable cause) {
        super(cause);
    }

    public UserDeleteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}

