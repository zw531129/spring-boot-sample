package org.example.mapper;

import org.example.model.Msg;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/** Created by zw531 on 2017/12/6. Usage: */
@Mapper
public interface MsgMapper {
  /** 查询所有 */
  @Select("SELECT * FROM msg")
  @Results({
    @Result(id = true, property = "id", column = "id"),
    @Result(property = "title", column = "title"),
    @Result(property = "content", column = "content")
  })
  List<Msg> findAll();

  /** 以id查询 */
  @Select("SELECT * FROM msg WHERE id = #{id}")
  Msg findById(@Param("id") Long id);

  /** 以title查询 */
  @Select("SELECT * FROM msg WHERE title = #{title}")
  Msg findByTitle(@Param("title") String title);

  /** 以id删除 */
  @Delete("DELETE FROM msg WHERE id = #{id}")
  void delete(Long id);

  /** 插入记录 */
  @Insert("INSERT INTO msg(title, content) VALUES (#{title}, #{content})")
  void add(Msg msg);

  /** 以id更新 */
  @Update("UPDATE msg SET title = #{title}, content = #{content} WHERE id = #{id}")
  void update(Msg msg);

  @Update(
      "CREATE table ${tableName}("
          + "id BIGINT primary key auto_increment,"
          + "title varchar(255),"
          + "content varchar(255)"
          + ");")
  void createTable(@Param("tableName") String tableName);
}
