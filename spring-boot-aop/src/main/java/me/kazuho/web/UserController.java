package me.kazuho.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by zw531 on 2018/1/13. Usage:
 */
@RestController
public class UserController {

  private static final Logger LOGGER = LoggerFactory.getLogger(UserController.class);

  @RequestMapping(value = "/login")
  public void login(@RequestParam("username") String username) {
    LOGGER.info(username + " logged in.");
  }

  @RequestMapping(value = "/logout")
  public void logout(@RequestParam("username") String username) {
    LOGGER.info(username + " logged out.");
  }
}
