package me.kazuho.timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by zw531 on 2018/1/12. Usage:
 */
@Component
public class SchduleTask {
  private static final SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
  private static final Logger LOGGER = LoggerFactory.getLogger(SchduleTask.class);

  @Scheduled(fixedRate = 5000)
  public void timeReporter() {
    LOGGER.info("now : " + sdf.format(new Date()));
  }
}
