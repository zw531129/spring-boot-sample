package me.kazuho;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
//    String byteCodePath = "/C:/Users/zw531/Code/spring-boot-sample/spring-boot-external-config/target/spring-boot-external-config-1.0-SNAPSHOT.jar!/BOOT-INF/classes!/";
//    int breakPoint = byteCodePath.indexOf(".jar");
//    String jarPath = byteCodePath.substring(0, breakPoint);
//    System.out.println(jarPath);
//    String deployPath = jarPath.substring(0, jarPath.lastIndexOf("/"));
//    System.out.println(deployPath);
//    String configPath = deployPath + "/config";
//    System.out.println(configPath);
  }
}
