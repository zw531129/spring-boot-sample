package org.example.constant;


public enum UserErrorCode {
    ADD_FAIL(50010, "testcase.error.add"),
    DELETE_FAIL(50011, "testcase.error.delete"),
    GET_DETAIL_FAIL(50012, "testcase.error.get_detail"),
    UPDATE_FAIL(50013, "testcase.error.update"),
    LIST_FAIL(50013, "testcase.error.list"),
    ;

    public final Integer code;
    public final String message;
    UserErrorCode(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

}
