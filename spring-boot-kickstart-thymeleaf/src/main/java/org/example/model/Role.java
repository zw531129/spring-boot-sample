package org.example.model;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by zw531 on 2018/4/27. Usage:
 */
@Entity
@Table(name = "role")
public class Role {
    @GeneratedValue
    @Id
    private Integer id;

    @NotNull
    @Column(name = "rid")
    private Integer rid;

    @NotEmpty
    @Column(name = "name")
    private String name;

    public Role() {
    }

    public Role(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "roles")
    private Set<Employee> employees = new HashSet<>();

    @Override
    public String toString() {
        return "Role{" +
                "id=" + id +
                ", rid=" + rid +
                ", name='" + name + '\'' +
                ", employees=" + employees +
                '}';
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRid() {
        return rid;
    }

    public void setRid(Integer rid) {
        this.rid = rid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Set<Employee> getEmployees() {
        return employees;
    }

    public void setEmployees(Set<Employee> employees) {
        this.employees = employees;
    }
}
