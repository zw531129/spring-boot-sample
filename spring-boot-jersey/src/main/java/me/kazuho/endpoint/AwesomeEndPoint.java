package me.kazuho.endpoint;

import com.google.gson.Gson;

import org.springframework.stereotype.Component;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import me.kazuho.model.Greeting;

@Component
@Path("/")
public class AwesomeEndPoint {
  @GET
  @Path("/hello")
  @Produces("application/json")
  public String greeting() {
    String from = "{words: \"Welcome\", numbers: [\"0\", \"1\", \"2\"]}";
    Gson gson = new Gson();

    Greeting greeting = gson.fromJson(from, Greeting.class);

    return gson.toJson(greeting);
  }
}
