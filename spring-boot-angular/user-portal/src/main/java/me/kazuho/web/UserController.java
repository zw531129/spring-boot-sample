package me.kazuho.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

import me.kazuho.model.User;
import me.kazuho.service.UserService;

@RestController
@RequestMapping("/users")
@CrossOrigin(origins = "*", maxAge = 3600)
public class UserController {
  @Autowired
  private UserService userService;

  @PostMapping
  public User create(@RequestBody User user) {
    return userService.create(user);
  }

  @GetMapping(path = "{/id}")
  public User findOne(@PathVariable("id") Integer id) {
    return userService.findById(id);
  }

  @PutMapping(path = "{/id}")
  public User update(@PathVariable Integer id, @RequestBody User user) {
    return userService.update(user);
  }

  @DeleteMapping(path = {"/{id}"})
  public User delete(@PathVariable("id") int id) {
    return userService.delete(id);
  }

  @GetMapping
  public List<User> findAll() {
    return userService.findAll();
  }

}