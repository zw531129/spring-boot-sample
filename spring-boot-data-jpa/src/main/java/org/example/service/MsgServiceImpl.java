package org.example.service;

import org.example.dao.MsgDao;
import org.example.model.Msg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/** Created by zw531 on 2018/1/4. Usage: */
@Service
@Transactional(isolation = Isolation.READ_COMMITTED)
public class MsgServiceImpl implements MsgService {
  @Autowired private MsgDao msgDao;

  @Override
  public void addMsg(Msg msg) {
    msgDao.saveAndFlush(msg);
  }

  @Override
  public List<Msg> getAllMsgs() {
    return msgDao.findAll();
  }

  @Override
  public void deleteMsg(Long id) {
    msgDao.deleteById(id);
  }

  @Override
  public Msg findByTitle(String title) {
    return msgDao.findByTitle(title);
  }

  public MsgDao getMsgDao() {
    return msgDao;
  }

  public void setMsgDao(MsgDao msgDao) {
    this.msgDao = msgDao;
  }
}
